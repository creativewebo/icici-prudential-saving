var winWidth,
    winHeight;


function setBackground() {
    $('.set-bg').each(function() {
        if (typeof $(this).attr('data-mob-img') === 'undefined') {
            $(this).css({
                'background': 'url(' + $(this).attr('data-img') + ')',
                'background-size': 'cover'
            });
        } else {
            if (winWidth > mobile_breakbpoint) {
                if (typeof $(this).attr('data-img') != 'undefined') {
                    $(this).css({
                        'background': 'url(' + $(this).attr('data-img') + ')',
                        'background-size': 'cover'
                    });
                }
            } else {
                $(this).css({
                    'background': 'url(' + $(this).attr('data-mob-img') + ')',
                    'background-size': 'cover'
                });
            }
        }
    });
}





function get_height_width() {
    winWidth = $(window).width(),
        winHeight = $(window).height();
}

function set_height_width() {
    if ($('body').height() < winHeight) {
        $('.wh').outerHeight(winHeight);
    }
    $('.wh-min').css('min-height', winHeight);
    $('.ww').outerWidth(winWidth);
}


function stickyMenu() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 60) {
            $('.bs-header').addClass("sticky");
            // $('body').css('paddingTop', '0');
        } else {
            $('.bs-header').removeClass("sticky");
        }
    });
}

function menuActive() {
    var m = $('.bs-header').attr('data-nav').toLowerCase();
    $('.main-nav li a').each(function(index) {
        if ($(this).html().toLowerCase() == m) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
}

function counter() {
    $(window).scroll(function() {
        if($(".mod-counter").length){
            var top_of_element = $(".mod-counter").offset().top;
            var bottom_of_element = $(".mod-counter").offset().top + $(".mod-counter").outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();
        
            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element))
            {
                // alert("Hi");
                $(".counter").each(function() {
                    var $this = $(this),
                        countTo = $this.attr("data-count");
                    $({ countNum: $this.text() }).animate({ countNum: countTo }, {
                        duration: 8000,
                        easing: "linear",
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                        }
                    });
                });
            } else {
                // alert("Bye");
                // the element is not visible, do something else
            }
        }
    });

}


function downArrow(){
    $(".cart-bx-parent .bs-icn-txt").off('click').on('click', function(){
        $(this).closest('.bs-carts').addClass('active');
        $(this).find('.dwn-arrow').hide();
        $(this).closest('.bs-carts').find('.accrdn-cards').show();
    });

    $(".cart-bx-parent .arrw-up").off('click').on('click', function(){
        $(this).closest('.bs-carts').removeClass('active');
        $(this).closest('.bs-carts').find('.accrdn-cards').hide();
        $(this).closest('.bs-carts').find('.dwn-arrow').show();
    });

      $(".la-edit").click(function(){
          $(".la-expand-col").show();
          $(".la-hide-bx").hide();
      })
      $(".la-save").click(function(){
        $(".la-expand-col").hide();
        $(".la-hide-bx").show();
    })

    $(".id-edit").click(function(){
        $(".id-expand-col").show();
        $(".id-hide-bx").hide();
    })
    $(".id-save").click(function(){
      $(".id-expand-col").hide();
      $(".id-hide-bx").show();
  })

}
function slide(){
    var swiper = new Swiper('.swiper-grow-wealth', {
        slidesPerView: 3,
        spaceBetween: 15,
        freeMode: true,
        // pagination: {
        //   el: '.swiper-pagination',
        //   clickable: true,
        // },
        breakpoints: {
            '992': {
              slidesPerView: 2,
            },
            '767': {
              slidesPerView: 2,
            //   spaceBetween: 20,
            }
        }
    
      });
}

var resize_timeout;

$(window).on('resize orientationchange', function(){
    clearTimeout(resize_timeout);

    resize_timeout = setTimeout(function(){
        $(window).trigger('resized');
    }, 250);
});

$(window).on('resized', function(){
    calculateMenuWidth();
    accordionMouseover();
});

function calculateMenuWidth(){
    if(($(window).width() < 768) && ($('body').hasClass('plans'))){
        var menu_width = 0;
        $(".filter-list .nav-tabs .nav-item").each(function(index, value)
        {
            menu_width = menu_width + Math.round($(this).width())
        })
        console.log(menu_width + 10);
        $(".filter-list .nav-tabs").css("width",menu_width + 10);
        //alert(menu_width);
    }
}

function accordionMouseover(){
    if($(window).width() > 768){
        $("i-calc .bs-accordian.typ-saving-plans-info .card").mouseover(function(){
            $("i-calc .bs-accordian.typ-saving-plans-info .card .card-header h2 .btn-link").addClass('collapsed');
            $("i-calc .bs-accordian.typ-saving-plans-info .card .collapse").removeClass('show');
            $(this).find('.btn-link').removeClass('collapsed');
            $(this).find('.collapse').addClass('show');
         });
    }
}

function scrollAnimations(){
    $(document).on('click', 'i-calc .bs-header .nav-btns', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        });
    });
}

function enableFooter(){
    var oldHeight = $(document).height();
    $(".disclaimer-box .btn-link").click(function(){
        setTimeout(function(){
            var NewHeight = $(document).height();
            // alert(oldHeight, NewHeight,(NewHeight > oldHeight));
            if(NewHeight > oldHeight){
                $('html, body').animate({
                    scrollTop: NewHeight
                }, 'slow');
            }
        },500);
    });
    
}
function scrollToTop() { 
    $(window).scrollTop(0); 
}
$(function() {
    // get_height_width();
    // set_height_width();
    setBackground();
    counter();
    downArrow();
    slide();
    //stickyMenu();
    scrollAnimations()
    accordionMouseover();
    enableFooter();
    scrollToTop();
     new WOW().init();

    calculateMenuWidth();

    $('.custom-dropdown').customSelect();

    $("#callModal .bs-toggle .bs-toggle-elements input:radio").change(function()
    {
        var selectedVal = $(this).val();
        $("#inputToday").val(selectedVal);
    });

    $(".sort-btn").click(function(){
        $(this).toggleClass('active');
    });

    $(".filter-box .bs-btn.type-white").click(function(){
        $(".filter-box .bs-btn.type-white").removeClass('active');
        $(this).addClass('active');
    });

    $("#close-grow-your-wealth").click(function(){
        $(".grow-your-wealth-content").toggleClass('hide');
    });
    
    $(".check-plans").click(function(e){
        e.preventDefault(); 
        $("#home-box").hide();
        $("#plans-box").show();
        $('body').addClass('plans').removeClass('landing-page');
        calculateMenuWidth();
    });

    $(".view-details").click(function(e){
        e.preventDefault();
        $("#plans-box").hide();
        $("#plan-detls-box").show();
        $('body').addClass('plan-detls').removeClass('plans');
    });

    $(".goto-index-from-plans").click(function(e){
        e.preventDefault();
        $("#plans-box").hide();
        $("#home-box").show();
        $('body').addClass('landing-page').removeClass('plans');
    });

    $(".goto-index-from-plans-details").click(function(e){
        e.preventDefault();
        $("#plan-detls-box").hide();
        $("#plans-box").show();
        $('body').addClass('plans').removeClass('landing-page');
    });

      $('#investModal').on('shown.bs.modal', function (e) {
        $("#investModal input[name=options]").change(function()
        {
            if($(this).prop("checked") == true){
                $("#inputToday").val($(this).val());
                $("#inputToday").attr('placeholder',$(this).val());
            }
        });    
    });
});



var mywindow = $(window);
var mypos = mywindow.scrollTop();
var up = false;
var newscroll;
mywindow.scroll(function () {
    newscroll = mywindow.scrollTop();
    if (newscroll > mypos && !up) {
        $('.filter-box').stop().slideToggle();
        up = !up;
        //console.log(up);
    } else if(newscroll < mypos && up) {
        $('.filter-box').stop().slideToggle();
        up = !up;
    }
    mypos = newscroll;
});