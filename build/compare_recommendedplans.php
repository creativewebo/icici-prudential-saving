<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ICICI Prudential</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- css group start -->
    <?php include_once 'view/include_css.html'; ?>
    <!-- css group end -->
</head>

<body class="wh-min plans">
    <i-calc>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header start -->
    <?php include_once 'view/header.html'; ?>
    <!-- header end -->

    <main class="plans">
        <section class="lyt-section filter-box">
            <div class="container">
                <div class="desk-filter-bx">
                    <div class="row">
                        <div class="col-md-12 mod-user-details">
                            <span class="text-uppercase">basic details:</span>
                            <span class=" black-text cm-mL5">Male | Age 30yrs</span>
                            <a href="index.php" class="blue-link cm-mL5">Edit Details</a>
                        </div>
                    </div>
                    <div class="row cm-mT30">
                        <div class="col-lg-4 col-md-6 mod-user-details">
                            <div class="row">
                                <div class="col-md-5 col-6">
                                    <div class="form-group">
                                        <label for="inputToday">Invest (in &#8377;)</label>
                                        <input type="email" class="form-control" id="inputToday" aria-describedby="emailHelp" placeholder="5,000">
                                    </div>
                                </div>
                                <div class="col-md-5  col-6">
                                    <div class="dropdown cm-mT30">
                                        <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                            <option value="monthly">Monthly</option>
                                            <option value="quaterly">Quaterly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mod-user-details">
                            <div class="form-group">
                                <label for="inputToday">Pay for</label>
                                <div class="btn-group btn-group-toggle bs-toggle" data-toggle="buttons">
                                    <label class="btn bs-btn type-white active">
                                        <input type="radio" name="years" id="option1" autocomplete="off" checked> 
                                        5 Yrs
                                    </label>
                                    <label class="btn bs-btn type-white">
                                        <input type="radio" name="years" id="option2" autocomplete="off">
                                        7 Yrs
                                    </label>
                                    <label class="btn bs-btn type-white">
                                        <input type="radio" name="years" id="option3" autocomplete="off">
                                        10 Yrs
                                    </label>
                                    </div>
                                <div class="row cm-mT10">
                                    <div class="col-md-4 col-5">
                                        <div class="btn-group btn-group-toggle bs-toggle" data-toggle="buttons">
                                            <label class="btn bs-btn type-white till-maturity">
                                                <input type="radio" name="years" id="option1" autocomplete="off" checked> 
                                                Till maturity
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-7">
                                        <div class="dropdown">
                                            <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                                <option value="default">Choose from below</option>
                                                <option value="quaterly">Quaterly</option>
                                                <option value="yearly">Yearly</option>
                                            </select>
                                        </div>                            
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mod-user-details">
                            <div class="row">
                                <div class="col-lg-8 offset-lg-3 col-md-8 offset-md-0">
                                    <label for="inputToday">Return after</label>
                                    <div class="dropdown">
                                        <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                            <option value="fifteen-years">15 Years</option>
                                            <option value="quaterly">Quaterly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mob-filter-bx">
                    <div class="row">
                        <div class="col-lg-12 mod-user-details">
                            <div class="row">
                                <div class="col-lg-5 col-md-6 col-6">
                                    <div class="form-group">
                                        <label for="input-val">Invest (in &#8377;)</label>
                                        <input type="email" class="form-control" id="input-val" aria-describedby="emailHelp" placeholder="5,000">
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-6  col-6">
                                    <div class="dropdown cm-mT30">
                                        <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                            <option value="monthly">Monthly</option>
                                            <option value="quaterly">Quaterly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12 col-6 mod-user-details">
                                    <div class="form-group">
                                        <label for="inputToday">Pay for</label>
                                                <div class="dropdown">
                                                    <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                                        <option value="default">5yrs</option>
                                                        <option value="quaterly">7yrs</option>
                                                        <option value="yearly">10yrs</option>
                                                    </select>
                                                </div>         
                                    </div>
                                </div>
                                <div class="col-lg-12 col-6 mod-user-details">
                                            <label for="inputToday">Return after</label>
                                            <div class="dropdown">
                                                <select name="" id="" class="btn bs-dropdown dropdown-toggle custom-dropdown">
                                                    <option value="fifteen-years">15 Years</option>
                                                    <option value="quaterly">Quaterly</option>
                                                    <option value="yearly">Yearly</option>
                                                </select>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mod-user-details text-center">
                            <span class="text-uppercase">View plans for</span>
                            <span class=" black-text cm-mL5">Male  | Age 30yrs</span>
                            <a href="index.php" class="blue-link cm-mL5">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="lyt-section filter-list"style="margin-top: 180px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="filter-heading">Recommended Plans for you</h2>
                    </div>
                </div>
                <div class="row filtered-list">
                    <div class="col-md-9 scrolling-box">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="ap-tab" data-toggle="tab" href="#app-plans" role="tab" aria-controls="app-plans" aria-selected="true">All Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="ep-tab" data-toggle="tab" href="#endowment-plans" role="tab" aria-controls="endowment-plans" aria-selected="false">Endowment Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="grp-tab" data-toggle="tab" href="#guaranteed-returns-plans" role="tab" aria-controls="guaranteed-returns-plans" aria-selected="false">Guaranteed Returns Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="ulp-tab" data-toggle="tab" href="#unit-link-plans" role="tab" aria-controls="unit-link-plans" aria-selected="false">Unit Link Plans</a>
                            </li>
                        </ul>
                    </div>   
                    <div class="col-md-3">
                        <button class="btn bs-button compare-btn">
                            <span class="icon-scale compare-icon"></span>
                            <span class="compare-text">Compare ULIP/Savings</span>
                            <div class="clearfix"></div>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row plans-list">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="filter-highlights">
                                    Get additional Guaranteed Returns on online purchase
                                </span>
                            </div>
                            <div class="col-md-6">
                                <button class="sort-btn">
                                    <span class="sort-text">Sort by:</span>
                                    <span class="icon-sort"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="app-plans" role="tabpanel" aria-labelledby="ap-tab">
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/assured-saving.png" alt="Assured Savings image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/future-perfect.png" alt="future perfect image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/wealth-builder.png" alt="Wealth Builder image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <button class="btn bs-btn grey-outline">
                                    Back
                                </button>
                            </div>
                            <div class="tab-pane fade" id="endowment-plans" role="tabpanel" aria-labelledby="ep-tab">
                            <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/assured-saving.png" alt="Assured Savings image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/future-perfect.png" alt="future perfect image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/wealth-builder.png" alt="Wealth Builder image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <button class="btn bs-btn grey-outline">
                                    Back
                                </button>
                            </div>
                            <div class="tab-pane fade" id="guaranteed-returns-plans" role="tabpanel" aria-labelledby="grp-tab">
                            <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/assured-saving.png" alt="Assured Savings image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/future-perfect.png" alt="future perfect image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/wealth-builder.png" alt="Wealth Builder image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <button class="btn bs-btn grey-outline">
                                    Back
                                </button>
                            </div>
                            <div class="tab-pane fade" id="unit-link-plans" role="tabpanel" aria-labelledby="ulp-tab">
                            <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/assured-saving.png" alt="Assured Savings image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/future-perfect.png" alt="future perfect image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="tab-item">
                                    <div class="white-header">
                                        <div class="plan-image">
                                            <a href="">
                                                <img src="assets/images/wealth-builder.png" alt="Wealth Builder image"  />
                                            </a>
                                        </div>
                                        <div class="plan-time text-center">
                                            <span class="line-1">Invest for</span>
                                            <b class="linr-2">5 Years</b>
                                        </div>
                                        <div class="plan-scheme">
                                            <a href="#">Save tax U/S 10(10D)</a>
                                        </div>
                                        <div class="big-data">
                                            <span class="heading-1">Maturity Amount</span>
                                            <span class="heading-2">₹ 15.7 Lakhs</span>
                                            <span class="icon-timer icon-information-circle-outline"></span>
                                            <div class="clearfix"></div>
                                            <span class="heading-3">(Returns @8%)</span>
                                        </div>
                                        <button type="button" class="btn bs-btn plan-trigger">Proceed</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grey-box">
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Savings with the comfort of guarantee</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Potential for wealth creation and get Life Cover</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="icon-content">
                                            <span class="icon-check-orange icon-box"></span>
                                            <span class="icon-text">Flexibility in premium payment term</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <a href="plan-detail.php" class="maroon-link">View Details</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <button class="btn bs-btn grey-outline">
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- footer start -->
    <?php include_once 'view/footer.html'?>
    <!-- footer end -->

    <!-- js group start -->
    <?php include_once 'view/include_js.html'?>
    <!-- js group end -->
    </i-calc>
</body>

</html>