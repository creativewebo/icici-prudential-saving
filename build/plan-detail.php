<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ICICI Prudential</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- css group start -->
    <?php include_once 'view/include_css.html'; ?>
    <!-- css group end -->
</head>

<body class="wh-min plan-detls">
    <i-calc>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header start -->
    <?php include_once 'view/header.html'; ?>
    <!-- header end -->

    <main class="home">
        <section class="lyt-plan-detls filter-list typ-pln-detls">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="ap-tab" data-toggle="tab" href="#plan-detls" role="tab" aria-controls="plan-detls" aria-selected="true">Plan details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ep-tab" data-toggle="tab" href="#illustration" role="tab" aria-controls="illustration" aria-selected="false">Benefit illustration </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link m-ryt-non" id="grp-tab" data-toggle="tab" href="#benefits" role="tab" aria-controls="benefits" aria-selected="false">Key Benefits</a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="col-md-4 download-brochure">
                                <div class="">
                                    <a href="assets/images/pdf/Get-well.pdf" type="submit" class="bs-btn typ-round typ-download-brochure" download>Download Brochure<i class="icon icon-brochure"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row plans-list">
                            <div class="col-md-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="plan-detls" role="tabpanel" aria-labelledby="ap-tab">
                                        <div class="bs-carts info-bx">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="info-headings">
                                                        <p class="top-title">Endowment plan</p>
                                                        <h2 class="heading">ICICI Pru Assured Savings Investment Plan</h2>
                                                        <p class="sub-heading">Minimum premium ₹2,500 p.m.</p>
                                                    </div>
                                                    <div class="bs-list typ-pln-dtls">
                                                        <ul>
                                                            <li><i class="icon icon-check-orange"></i>Guaranteed Additions ever year</li>
                                                            <li><i class="icon icon-check-orange"></i>Guaranteed Maturity Benefit - Lumpsum at maturity</li>
                                                            <li><i class="icon icon-check-orange"></i>Guaranteed Protection - Life cover</li>
                                                            <li><i class="icon icon-check-orange"></i>Tax benefits u/s 80(C) and 10(10)D</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 stamp">
                                                    <img src="assets/images/stamp.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bs-carts">
                                            <div class="la-hide-bx">
                                                <div class="row">
                                                    <div class="col-md-9 col-8 ">
                                                        <p class="title">Life Assured Basic Details</p>
                                                        <p class="subtitle"><span>Gender: Male</span><span>Date of birth: 10/12/1090</span> <span>mobile: 9888765432</span></p>
                                                    </div>
                                                    <div class="col-md-3  col-4 edit-btn-bx">
                                                    <button type="submit" class="bs-btn typ-edit la-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="la-expand-col">
                                                <div class="row">
                                                    <div class="col-md-10 col-12">
                                                        <div class="row">
                                                            <div class="col-md-10 col-8">
                                                                <p class="title">Life Assured Basic Details</p>
                                                            </div>
                                                            <!-- <div class="col-md-2 col-4">
                                                                <button type="submit" class="bs-btn typ-edit la-save">Save</button>
                                                            </div> -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8 col-12">
                                                                <div class="bs-form typ-custmr-dtls typ-pln-dtl-accrdn">
                                                                    <form>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <label for="inputToday">Gender</label>
                                                                            </div>
                                                                            <div class="col-md-12 gender-check radio-toolbar">
                                                                                <div class="gender-check-wrapper js-gender suitablity_gender">

                                                                                    <input type="radio" id="Male" name="gender" value="Male" checked="">
                                                                                    <label class="slide-left delay02" for="Male"><span class="checkmark"></span>Male</label>

                                                                                    <input type="radio" id="Female" name="gender" value="Female">
                                                                                    <label class="slide-left delay03" for="Female"><span class="checkmark"></span>Female</label>

                                                                                    <input type="radio" id="Other" name="gender" value="Other">
                                                                                    <label class="slide-left delay04" for="Other"><span class="checkmark"></span>Other</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group bs-field has-error">
                                                                                    <label for="inputToday">Date of Birth</label>
                                                                                    <input type="text" class="form-control calculate-age"  aria-describedby="emailHelp" placeholder="dd/mm/yyyy" >
                                                                                    <span class="field-right-label age-calculated">XX Years</span>
                                                                                    <span class="error-label">Enter your date of birth</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="inputToday">Mobile</label>
                                                                                        <input type="number" class="form-control" id="inputToday" aria-describedby="emailHelp" placeholder="10 digit mobile number">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="input-email">Email</label>
                                                                                        <input type="email" class="form-control" id="input-email" aria-describedby="emailHelp" placeholder="abc123@gmail.com">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 col-4 edit-btn-bx typ-flex-start">
                                                                <button type="submit" class="bs-btn typ-edit la-save">Save</button>
                                                            </div>
                                                </div>                                               
                                            </div>
                                        </div>
                                        <div class="bs-carts">
                                            <div class="id-hide-bx">
                                                <div class="row">
                                                    <div class="col-md-9 col-8 ">
                                                        <p class="title">Investment details</p>
                                                        <p class="subtitle"><span>Investment amount: 2,500 monthly</span><span>Pay for 5 yrs</span> <span>withdraw after 10yrs</span></p>
                                                    </div>
                                                    <div class="col-md-3  col-4 edit-btn-bx">
                                                    <button type="submit" class="bs-btn typ-edit id-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="id-expand-col">
                                                <div class="row">
                                                    <div class="col-md-10 col-12">
                                                        <div class="row">
                                                            <div class="col-md-10 col-8">
                                                                <p class="title">Life Assured Basic Details</p>
                                                            </div>
                                                            <!-- <div class="col-md-2 col-4">
                                                                <button type="submit" class="bs-btn typ-edit la-save">Save</button>
                                                            </div> -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8 col-12">
                                                                <div class="bs-form typ-custmr-dtls typ-pln-dtl-accrdn">
                                                                    <form>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <label for="inputToday">Gender</label>
                                                                            </div>
                                                                            <div class="col-md-12 gender-check radio-toolbar">
                                                                                <div class="gender-check-wrapper js-gender suitablity_gender">

                                                                                    <input type="radio" id="male-val" name="gender" value="Male" checked="">
                                                                                    <label class="slide-left delay02" for="male-val"><span class="checkmark"></span>Male</label>

                                                                                    <input type="radio" id="female-val" name="gender" value="Female">
                                                                                    <label class="slide-left delay03" for="female-val"><span class="checkmark"></span>Female</label>

                                                                                    <input type="radio" id="c" name="gender" value="Other">
                                                                                    <label class="slide-left delay04" for="female-val"><span class="checkmark"></span>Other</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group bs-field has-error">
                                                                                    <label for="inputToday">Date of Birth</label>
                                                                                    <input type="text" class="form-control calculate-age"  aria-describedby="emailHelp" placeholder="dd/mm/yyyy" >
                                                                                    <span class="field-right-label age-calculated">XX Years</span>
                                                                                    <span class="error-label">Enter your date of birth</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="mobile-field">Mobile</label>
                                                                                        <input type="number" class="form-control" id="mobile-field" aria-describedby="emailHelp" placeholder="10 digit mobile number">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="email-field">Email</label>
                                                                                        <input type="email" class="form-control" id="email-field" aria-describedby="emailHelp" placeholder="abc123@gmail.com">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 col-4 edit-btn-bx typ-flex-start">
                                                        <button type="submit" class="bs-btn typ-edit id-save">Save</button>
                                                    </div>
                                                </div>                                               
                                            </div>
                                        </div>
                                        <!-- <div class="bs-carts">
                                            <div class="row">
                                                <div class="col-md-9  col-8">
                                                    <p class="title">Investment details</p>
                                                    <p class="subtitle"><span>Investment amount: 2,500 monthly</span><span>Pay for 5 yrs</span> <span>withdraw after 10yrs</span></p>
                                                </div>
                                                <div class="col-md-3  col-4  edit-btn-bx">
                                                    <button type="submit" class="bs-btn typ-edit" data-toggle="modal" data-target="#investModal">Edit</button>
                                                </div>
                                            </div>
                                        </div> -->
                                         <div class="col-md-4 mob-download-brochure">
                                            <div class="">
                                                <a href="assets/images/pdf/Get-well.pdf" type="submit" class="bs-btn typ-round typ-download-brochure" download>Download Brochure<i class="icon icon-brochure"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="illustration" role="tabpanel" aria-labelledby="ep-tab">
                                            <div class="bs-carts info-bx">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="info-headings">
                                                            <p class="top-title">Benefit illustration</p>
                                                            <h2 class="heading">ICICI Pru Assured Savings Investment Plan</h2>
                                                            <p class="sub-heading">Minimum premium ₹3000 p.m.</p>
                                                        </div>
                                                        <div class="bs-list typ-pln-dtls">
                                                            <ul>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Additions ever year</li>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Maturity Benefit - Lumpsum at maturity</li>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Protection - Life cover</li>
                                                                <li><i class="icon icon-check-orange"></i>Tax benefits u/s 80(C) and 10(10)D</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3"></div>
                                                </div>
                                            </div>
                                            <div class="bs-carts">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <p class="title">Life Assured Basic Details</p>
                                                        <p class="subtitle"><span>Gender: Male</span><span>Date of birth: 10/12/1090</span>    <span>mobile: 9888765432</span></p>
                                                    </div>
                                                    <div class="col-md-3 edit-btn-bx">
                                                    <button type="submit" class="bs-btn  typ-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-carts">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <p class="title">Investment details</p>
                                                        <p class="subtitle"><span>Investment amount: 3,500 monthly</span><span>Pay for 5 yrs</span> <span>withdraw after 10yrs</span></p>
                                                    </div>
                                                    <div class="col-md-3  edit-btn-bx">
                                                        <button type="submit" class="bs-btn typ-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <!-- <div class="tab-pane fade" id="benefits" role="tabpanel" aria-labelledby="grp-tab">
                                            <div class="bs-carts info-bx">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="info-headings">
                                                            <p class="top-title">Key Benefits</p>
                                                            <h2 class="heading">ICICI Pru Assured Savings Investment Plan</h2>
                                                            <p class="sub-heading">Minimum premium ₹2,500 p.m.</p>
                                                        </div>
                                                        <div class="bs-list typ-pln-dtls">
                                                            <ul>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Additions ever year</li>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Maturity Benefit - Lumpsum at maturity</li>
                                                                <li><i class="icon icon-check-orange"></i>Guaranteed Protection - Life cover</li>
                                                                <li><i class="icon icon-check-orange"></i>Tax benefits u/s 80(C) and 10(10)D</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3"></div>
                                                </div>
                                            </div>
                                            <div class="bs-carts">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <p class="title">Life Assured Basic Details</p>
                                                        <p class="subtitle"><span>Gender: Male</span><span>Date of birth: 10/12/1090</span>    <span>mobile: 9888765432</span></p>
                                                    </div>
                                                    <div class="col-md-3 edit-btn-bx">
                                                    <button type="submit" class="bs-btn  typ-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-carts">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <p class="title">Investment details</p>
                                                        <p class="subtitle"><span>Investment amount: 2,500 monthly</span><span>Pay for 5 yrs</span> <span>withdraw after 10yrs</span></p>
                                                    </div>
                                                    <div class="col-md-3  edit-btn-bx">
                                                        <button type="submit" class="bs-btn typ-edit">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="sticky-div">
                            <div class="row">
                                <div class="col-lg-12 col-md-6 order-md-1 order-2">
                                    <div class="maturity-amunt-info mob-non">
                                        <h3>Maturity Amount</h3>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <p>₹ 3.25 Lakh</p>
                                                <p>@4%ARR</p>
                                            </div>
                                            <div class="col-md-1 col-1 seprator">
                                                <span class="vertical-line"></span>
                                                <span class="round">OR</span>
                                            </div>
                                            <div class="col-md-5 col-5">
                                                <p>₹ 3.25 Lakh</p>
                                                <p>@4%ARR</p>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="bs-btn" data-toggle="modal" data-target="#buyPlanForModal">Invest <span>&#x20B9;</span> 5,000 monthly<i class="icon icon-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 order-md-2 order-1">
                                    <div class="row icn-title">
                                        <div class="col-md-2 col-4 shield-icn"><i class="icon icon-shield"></i></div>
                                        <div class="col-md-10 col-8 p-non">You can depend on us</div>
                                    </div>
                                    <div class="row description">
                                        <div class="col-md-12"><span>1.29 Lakh Cr</span> Benefits paid up to March 31, 2019</div>
                                    </div>
                                    <div class="row description">
                                        <div class="col-md-12"><span>30 Day Return Policy,</span> if not satisfied, you can return (free look) the policy and get your money back.</div>
                                    </div>
                                    <div class="row description">
                                        <div class="col-md-12"><span>Service Assurance,</span> your policy will be issued in 72 hours on receipt of all documents</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 cntr-align">
                        <a href="index.php" type="submit" class="bs-btn close-btn typ-back">Back</a>
                    </div>
                    <div class="col-md-4 show-in-mob">
                        <div class="row">
                            <div class="col-md-12 order-md-1 order-2">
                                <div class="maturity-amunt-info">
                                    <h3>Maturity Amount</h3>
                                    <div class="row">
                                        <div class="col-md-5 col-5">
                                            <p>₹ 3.25 Lakh</p>
                                            <p>@4%ARR</p>
                                        </div>
                                        <div class="col-md-1 col-1 seprator">
                                            <span class="vertical-line"></span>
                                            <span class="round">OR</span>
                                        </div>
                                        <div class="col-md-5 col-5">
                                            <p>₹ 3.25 Lakh</p>
                                            <p>@4%ARR</p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="bs-btn" data-toggle="modal" data-target="#buyPlanForModal">Invest<span>&#x20B9;</span> 5,000 monthly<i class="icon icon-arrow-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


 <!-- Modal -->
    <div class="modal fade bs-popup type-personal-details" id="personalDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Provide us your following details</h5>
            <h6 class="modal-sub-title" id="exampleModalLabel">This would help us recommend you the best plans we offer</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span class="icon-close-circle" aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">
            <form action="">
            <div class="bs-form typ-custmr-dtls">
            <label>Gender</label>
            <div class="row three-grid">
              <div class="col-md-12 gender-check radio-toolbar">
                <div class="gender-check-wrapper js-gender suitablity_gender">
                  <input type="radio" id="male-radio" name="gender" value="Male" checked="">
                  <label class="slide-left delay02" for="male-radio"><span class="checkmark"></span>Male</label>

                  <input type="radio" id="female-radio" name="gender" value="Female">
                  <label class="slide-left delay03" for="female-radio"><span class="checkmark"></span>Female</label>

                  <input type="radio" id="other-radio" name="gender" value="Other">
                  <label class="slide-left delay04" for="other-radio"><span class="checkmark"></span>Other</label>
                </div>
              </div>
              </div>
            </div>
            <div class="form-group bs-field has-error">
                <label for="calculate-age">Date of Birth</label>
                <input type="dob" class="form-control calculate-age"  aria-describedby="emailHelp" placeholder="dd/mm/yyyy">
                <span class="field-right-label age-calculated">XX Years</span>
                <span class="error-label">Enter your date of birth</span>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label for="personal-mobile-field">Mobile</label>
                  <input type="number" class="form-control" id="personal-mobile-field" aria-describedby="emailHelp" placeholder="10 digit mobile number">
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="inputTime">Email</label>
                  <input type="email" class="form-control" id="inputTime" aria-describedby="emailHelp" placeholder="abc@123.com">
                </div>
              </div>
            </div>
            
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn bs-btn " data-dismiss="modal">Show Plans</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade bs-popup type-investModal" id="investModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              <span>How much would you like to invest per</span>
              <select name="" id="" class="bs-dropdown type-underline type-blue custom-dropdown">
                <option value="month">month</option>
                <option value="yea">year</option>
              </select>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span class="icon-close-circle" aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">
            <form action="">
            <div class="form-group">
                <input type="email" class="form-control bs-dropdown type-underline type-small" id="invest" aria-describedby="emailHelp" placeholder="&#8377; 5,000">
            </div>
            <div class="btn-group btn-group-toggle bs-toggle" data-toggle="buttons">
              <label class="btn active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> 
                  <span>&#8377;</span>
                  <span>2,500</span>
              </label>
              <label class="btn">
                <input type="radio" name="options" id="option2" autocomplete="off">
                <span>&#8377;</span>
                <span>5,000</span>
              </label>
              <label class="btn">
                <input type="radio" name="options" id="option3" autocomplete="off">
                <span>&#8377;</span>
                <span>10,000</span>
              </label>
            </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn bs-btn" data-dismiss="modal">Show Returns</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade bs-popup type-buy-plan" id="buyPlanForModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Whom are you buying this plan for?</h5>
            <h6 class="modal-sub-title" id="exampleModalLabel">This would help us recommend you the best plans we offer</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span class="icon-close-circle" aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">
            <form action="">
                <div class="bs-form typ-custmr-dtls">
                    <div class="row">
                        <div class="col-md-12 gender-check radio-toolbar text-center">
                            <div class="gender-check-wrapper js-gender suitablity_gender full-width">
                                <input type="radio" id="male-value" name="self" value="Male" checked="">
                                <label class="slide-left delay02" for="male-value">
                                <span class="checkmark"></span>Self
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row three-grid">
                      <div class="col-md-12 gender-check radio-toolbar">
                        <div class="gender-check-wrapper js-gender suitablity_gender">
                          <input type="radio" id="Spouse" name="gender" value="Spouse" checked="">
                          <label class="slide-left delay02" for="Spouse">
                            <span class="checkmark"></span> Spouse
                          </label>
                          <input type="radio" id="Child" name="gender" value="Child">
                          <label class="slide-left delay03" for="Child">
                            <span class="checkmark"></span> Child
                          </label>
                          <input type="radio" id="Grandchild" name="gender" value="Grandchild">
                          <label class="slide-left delay04 grandchild" for="Grandchild">
                            <span class="checkmark"></span> Grandchild
                          </label>
                        </div>
                      </div>
                    </div>
                </div>
            </form>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn bs-btn " data-dismiss="modal">Show Plans</button>
          </div>
        </div>
      </div>
    </div>


      <!-- footer start -->
      <?php include_once 'view/footer.html'?>
    <!-- footer end -->

    <!-- js group start -->
    <?php include_once 'view/include_js.html'?>
    <!-- js group end -->
    </i-calc>
</body>

</html>